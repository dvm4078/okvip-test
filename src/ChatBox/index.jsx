import { useState, useEffect, useRef } from 'react';
import classes from './index.module.scss';

import LoadingImg from '../assets/loading.svg'

const ChatBox = ({ isConnected, addComment, chats }) => {
  const chatContentElmRef = useRef()
  const [comment, setComment] = useState('');
  const onSubmit = (event) => {
    event.preventDefault();
    addComment(comment);
    setComment('')
  }

  useEffect(() => {
    chatContentElmRef.current.scrollTop = chatContentElmRef.current.scrollHeight;
  }, [chats])

  return <div className={classes.chatBox}>
    {!isConnected && (
      <div className={classes.loading}>
        <img src={LoadingImg} width={50} />
      </div>
    )}
    <div className={classes.chatContent} ref={chatContentElmRef}>
      {chats.map(({ id, userId, message }) => (
        <div key={id} className={classes.comment}>
          <div className={classes.avatar}>
            {userId[0]}
          </div>
          <div className={classes.content}>
            <label>{userId}</label>
            <span>{message}</span>
          </div>
        </div>
      ))}
    </div>
    <div className={classes.sendBox}>
      <form onSubmit={onSubmit}>
        <input
          disabled={!isConnected}
          value={comment}
          onChange={(event) => setComment(event.target.value)}
        />
        <button disabled={!comment || !isConnected} type='submit'>
          Gửi
        </button>
      </form>
    </div>
  </div>
}

export default ChatBox