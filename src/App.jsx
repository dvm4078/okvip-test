import { useState, useRef, useEffect, useLayoutEffect } from 'react'
import { IconContext } from "react-icons";
import { v4 as uuidv4 } from 'uuid';

import 'normalize.css';
import classes from './App.module.scss';

import ChatBox from './ChatBox';
import Player from './Player';
import { socket } from './socket';

const UUID_KEY = 'uuid_'

const sources = {
  mp4: {
    type: 'video/mp4',
    url: 'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4'
  },
  flv: {
    type: 'video/x-flv',
    url: 'https://flvplayer.js.org/assets/video/weathering-with-you.flv'
  },
  m3u8: {
    type: 'application/x-mpegURL',
    url: 'https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8'
  }
}

function App() {
  const highlightCommentBoxElmRef = useRef()
  const [isShowHighlightComment, setIsShowHighlightComment] = useState(true);
  const [videoType, setVideoType] = useState('mp4');
  const [chats, setChats] = useState([])
  const [isConnected, setIsConnected] = useState(socket.connected);

  const toggleShowHighlightComment = () => {
    setIsShowHighlightComment(!isShowHighlightComment);
  }

  const randomInteger = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  const showHighlightComment = (comment) => {
    const marqueeElm = document.createElement("marquee");
    marqueeElm.innerHTML = comment.message;
    marqueeElm.id = comment.id;
    marqueeElm.loop = 1;
    marqueeElm.style.top = randomInteger(50, 300) + 'px'
    // marqueeElm.onfinish = function () {
    //   document.getElementById(comment.id).remove();
    // }
    highlightCommentBoxElmRef.current.append(marqueeElm);
    setTimeout(() => {
      document.getElementById(comment.id).remove();
    }, 45000)
  }

  useEffect(() => {
    if (!localStorage.getItem(UUID_KEY)) {
      localStorage.setItem(UUID_KEY, uuidv4())
    }
    function onConnect() {
      setIsConnected(true);
    }

    function onDisconnect() {
      setIsConnected(false);
    }

    function newChat(newChat) {
      setChats((chats) => [...chats, newChat]);
      showHighlightComment({
        id: newChat.id,
        message: newChat.message
      })
    }

    socket.on('connect', onConnect);
    socket.on('disconnect', onDisconnect);
    socket.on('new chat', newChat);

    socket.on('broadcast', m => console.log('Received broadcast:', m));
    return () => {
      socket.off('connect', onConnect);
      socket.off('disconnect', onDisconnect);
      socket.off('new chat', newChat);
    };
  }, []);

  const addComment = (message) => {
    socket.emit('new chat', {
      userId: localStorage.getItem(UUID_KEY),
      message
    });
  }

  return (
    <IconContext.Provider value={{ color: "white", size: '24px' }}>
      <div className={classes.container}>
        <div className={classes.videoBox}>
          {/* <div className={classes.roomInfo}>

          </div> */}
          <div className={classes.roomPlayer}>
            <div
              className={classes.highlightCommentBox}
              ref={highlightCommentBoxElmRef}
              style={{ opacity: isShowHighlightComment ? 1 : 0 }}
            />
            <Player
              isShowHighlightComment={isShowHighlightComment}
              toggleShowHighlightComment={toggleShowHighlightComment}
              videoType={sources[videoType].type}
              videoSource={sources[videoType].url}
            />
          </div>
          <div className={classes.giftShop}>
            {['mp4', 'flv', 'm3u8'].map((type) => (
              <button key={type} onClick={() => setVideoType(type)} className={videoType === type ? classes.active : ''}>
                {type.toUpperCase()}
              </button>
            ))}
          </div>
        </div>
        <ChatBox
          isConnected={isConnected}
          addComment={addComment}
          chats={chats}
        />
      </div>
    </IconContext.Provider>
  )
}

export default App
