import React, { useState, useRef, memo, useEffect } from 'react'
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import {
  LuPause,
  LuPlay,
  LuRotateCcw,
  LuVolumeX,
  LuVolume2,
  LuCalendarRange,
  LuCalendarX2,
  LuCast,
  LuMaximize2,
  LuMinimize2,
  LuPlayCircle,
  LuPauseCircle
} from "react-icons/lu";

import classes from './index.module.scss';
import PosterImg from '../assets/poster.jpeg'

const Player = memo(function Player({
  isShowHighlightComment,
  toggleShowHighlightComment,
  videoType,
  videoSource
}) {
  const videoElmRef = useRef();
  const playbackElmRef = useRef();
  const currentVolume = useRef();
  const progressBarElmRef = useRef();
  const seekElmRef = useRef();
  const timeElapsedElmRef = useRef();
  const durationElmRef = useRef();

  const [isPlaying, setIsPlaying] = useState(false);
  const [volume, setVolume] = useState(100);
  const [isPip, setIsPip] = useState(false);
  const [isFullScreen, setIsFullScreen] = useState(false);

  const handle = useFullScreenHandle();

  const formatTime = (timeInSeconds) => {
    const result = new Date(timeInSeconds * 1000).toISOString().substr(11, 8);

    return {
      minutes: result.substr(3, 2),
      seconds: result.substr(6, 2),
    };
  }

  const initializeVideo = () => {
    const videoDuration = Math.round(videoElmRef.current.duration);
    const time = formatTime(videoDuration);
    durationElmRef.current.innerText = `${time.minutes}:${time.seconds}`

    seekElmRef.current.setAttribute('max', videoDuration);
    progressBarElmRef.current.setAttribute('max', videoDuration);
    seekElmRef.current.value = 0;
    progressBarElmRef.current.value = 0;
  }

  const animatePlayback = () => {
    playbackElmRef.current.animate(
      [
        {
          opacity: 1,
          transform: 'scale(1)',
        },
        {
          opacity: 0,
          transform: 'scale(1.3)',
        },
      ],
      {
        duration: 500,
      }
    );
  }

  const toggePlay = () => {
    if (videoElmRef.current.paused || videoElmRef.current.ended) {
      videoElmRef.current.play();
      setIsPlaying(true);
    } else {
      videoElmRef.current.pause();
      setIsPlaying(false);
    }
    animatePlayback();
  }

  const restart = () => {
    videoElmRef.current.pause();
    videoElmRef.current.currentTime = 0;
    videoElmRef.current.load();
    videoElmRef.current.play();
    setIsPlaying(true);
  }

  const onChangeVolume = (e) => {
    const newVolume = e.target.value
    setVolume(newVolume)
    currentVolume.current = newVolume;
    videoElmRef.current.volume = newVolume;
  }

  const toggleMute = () => {
    let newVolume
    if (!volume) {
      newVolume = currentVolume.current || 100
    } else {
      newVolume = 0;
    }
    setVolume(newVolume)
    videoElmRef.current.volume = newVolume;
  }

  // const toggleShowHighlightComment = () => {
  //   setIsShowHighlightComment(!isShowHighlightComment);
  // }

  const togglePip = () => {
    if (videoElmRef.current !== document.pictureInPictureElement) {
      videoElmRef.current.requestPictureInPicture();
      setIsPip(true)
    } else {
      document.exitPictureInPicture();
      setIsPip(false);
    }
  }

  const toggleFullScreen = () => {
    if (isFullScreen) {
      handle.exit();
    } else {
      handle.enter();
    }
  }

  const onTimeUpdate = () => {
    const time = formatTime(Math.round(videoElmRef.current.currentTime));
    timeElapsedElmRef.current.innerText = `${time.minutes}:${time.seconds}`;
    seekElmRef.current.value = Math.floor(videoElmRef.current.currentTime);
    progressBarElmRef.current.value = Math.floor(videoElmRef.current.currentTime);
  }

  const skipAhead = (event) => {
    const skipTo = event.target.dataset.seek
      ? event.target.dataset.seek
      : event.target.value;
    videoElmRef.current.currentTime = skipTo;
    progressBarElmRef.current.value = skipTo;
    seekElmRef.current.value = skipTo;
  }

  useEffect(() => {
    prepareVideo();
  }, [videoSource])

  const prepareVideo = () => {
    var videoElement = document.getElementById('player');
    if (videoType === 'video/x-flv' && window.flvjs?.isSupported()) {
      var flvPlayer = window.flvjs.createPlayer({
        type: 'flv',
        url: videoSource
      });
      flvPlayer.attachMediaElement(videoElement);
      flvPlayer.load();
    } else if (videoType === 'application/x-mpegURL' && window.Hls?.isSupported()) {
      var hls = new window.Hls();
      hls.loadSource(videoSource);
      hls.attachMedia(videoElement);
    } else {
      videoElement.removeAttribute('src')
      videoElement.load();
    }
    setIsPlaying(false);
  }

  return (
    <FullScreen
      handle={handle}
      className={classes.videoContainer}
      onChange={setIsFullScreen}>
      <video
        id="player"
        poster={PosterImg}
        ref={videoElmRef}
        className={classes.video}
        preload="metadata"
        onEnded={() => {
          seekElmRef.current.value = 0;
          progressBarElmRef.current.value = 0;
          setIsPlaying(false);
        }}
        onTimeUpdate={onTimeUpdate}
        onLoadedMetadata={initializeVideo}
      >
        {/* <source src="https://flvplayer.js.org/assets/video/weathering-with-you.flv" type="video/x-flv"></source> */}
        {/* <source src="https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8" type="application/x-mpegURL"></source> */}
        <source src={videoSource} type={videoType}></source>
      </video>
      <div className={classes.playbackAnimation} ref={playbackElmRef}>
        {isPlaying ? <LuPlayCircle size="48px" /> : <LuPauseCircle size="48px" />}
      </div>
      <div className={classes.videoControls}>
        <div className={classes.videoProgress}>
          <progress ref={progressBarElmRef} max="33" value="0" />
          <input
            ref={seekElmRef}
            onChange={skipAhead}
            min="0"
            type="range"
            step="1"
            max="33"
            value="0"
          />
        </div>
        <div className={classes.bottomControls}>
          <div className={classes.block}>
            <div className={classes.controlItem} onClick={toggePlay}>
              {isPlaying ? <LuPause /> : <LuPlay />}
            </div>
            <div className={classes.controlItem} onClick={restart}>
              <LuRotateCcw />
            </div>
            <div className={classes.time}>
              <time ref={timeElapsedElmRef}>00:00</time>
              <span> / </span>
              <time ref={durationElmRef}>00:00</time>
            </div>

          </div>
          <div className={classes.block} style={{ gap: '8px' }}>
            <div className={classes.volumeControl}>
              <div className={classes.controlItem} onClick={toggleMute}>
                {volume ? <LuVolume2 /> : <LuVolumeX />}
              </div>
              <input
                className={classes.volume}
                value={volume}
                type="range"
                max="1"
                min="0"
                step="0.01"
                data-volume="1"
                onChange={onChangeVolume}
              />
            </div>
            <div className={classes.controlItem} onClick={toggleShowHighlightComment}>
              {isShowHighlightComment ? <LuCalendarRange /> : <LuCalendarX2 />}
            </div>
            <div className={classes.controlItem} onClick={togglePip}>
              <LuCast />
            </div>
            <div className={classes.controlItem} onClick={toggleFullScreen}>
              {isFullScreen ? <LuMinimize2 /> : <LuMaximize2 />}
            </div>
          </div>
        </div>
      </div>
    </FullScreen>
  )
})



export default Player
